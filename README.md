Use Raspberry PI to control LCD driven by st7735.  
Use the lcd.luma lib(https://pypi.org/project/luma.lcd/).  
Use Juhe.com to gather the weather information.  
Copy config.yml.example to config.yml and change the content.  
```
cp config.yml.example config.yml
vi config.yml
python3 status.py
```

Notice that I'm using SPI2 to drive the screen, 
if you're using default SPI port, change the parameters in status.py line 17 & 18  
e.g.  

```
serial = spi(port=0, device=0)
device = st7735(serial, width=128, height=128, rotate=0, h_offset=1, v_offset=2, bgr=True)
```


Working with Raspberry Pi 2B and python3.  
Should work with Raspberry Pi 2/3/4 and python2, but I don't have the device to test it.  