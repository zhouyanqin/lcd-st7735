import os
import re
import psutil
import time
import requests
import sqlite3
import yaml

BOOT_TIME = psutil.boot_time()


def get_ip_address(ifname):
    try:
        address = os.popen("ifconfig | grep -A 1 %s|tail -1| awk '{print $2}'" % ifname).read()
        return re.search(r'(?<![\.\d])(?:25[0-5]\.|2[0-4]\d\.|[01]?\d\d?\.)'
                         r'{3}(?:25[0-5]|2[0-4]\d|[01]?\d\d?)(?![\.\d])',address).group()
    except:
        return "Err"


def get_up_time():
    up = time.time() - BOOT_TIME

    return sec2day(up)


# return datetime.datetime.fromtimestamp(up).strftime('%d-%H%M%S')


def sec2day(seconds):
    num1, a = divmod(seconds, 86400)
    num2, a = divmod(a, 3600)
    num3, num4 = divmod(a, 60)
    return '%02dd%02dh%02dm%02ds' % (num1, num2, num3, num4)


def get_public_ip():
    try:
        r = requests.get(url="http://members.3322.org/dyndns/getip")
        return '%s' % (r.text.strip())
    except:
        return "--"


def get_cpu_temperture():
    with open('/sys/class/thermal/thermal_zone0/temp', 'r') as f:
        return '%.3f' % (float(f.read().strip()) / 1000)


def get_cpu_percent(interval=None, percpu=True):
    if percpu:
        list = psutil.cpu_percent(interval=interval, percpu=percpu)
        list = [str(i) for i in list]
        return " ".join(list)
    else:
        return str(psutil.cpu_percent(interval=interval, percpu=percpu))


def get_weather():
    try:
        with open(os.path.abspath(os.path.join(os.path.dirname(__file__), 'config.yml')), 'r') as f:
            appKey = yaml.safe_load(f.read())['juhe']['appKey']
    except Exception as arg:
        return ['读取配置错误', arg]
    table_weather_ids = 'jh_weather_ids'
    try:
        #从sqlite里面获取天气类型
        conn = sqlite3.connect(os.path.abspath(os.path.join(os.path.dirname(__file__), '.status.db')))
        wids = {}
        if not table_exists(conn=conn, table_name=table_weather_ids):
            #表不存在，创建表
            conn.execute('CREATE TABLE ' + table_weather_ids + '(wid VARCHAR(10) PRIMARY KEY, weather VARCHAR(20));')
        else:
            for row in conn.execute('SELECT * FROM ' + table_weather_ids):
                wids[row['wid']] = row['weather']
        if len(wids) == 0 :
            # 说明没有，需要请求
            r = requests.get(url="http://apis.juhe.cn/simpleWeather/wids?key=" + appKey)
            r.encoding = 'utf-8'
            if r.json()['error_code'] == 0:
                data = r.json()['result']
                conn.executemany('INSERT INTO ' + table_weather_ids + ' VALUES(?, ?)', [(i['wid'], i['weather'])for i in data])
                for row in data:
                    wids[row['wid']] = row['weather']
            else:
                return ['获取天气类型失败']
        # 获取天气数据
        r = requests.get(url="http://apis.juhe.cn/simpleWeather/query?city=1&key=" + appKey)
        r.encoding = 'utf-8'
        if r.json()['error_code'] != 0:
            return ['获取天气信息失败！']
        data = r.json()['result']
        result = []
        #实时天气
        result.append(data['city'] + "@实时: " + data['realtime']['temperature'] + "℃, " + wids[data['realtime']['wid']] +
                      "\n" + data['realtime']['direct'] + data['realtime']['power'] + "\n湿度:" + data['realtime']['humidity'] +
                      ", AQI:" + data['realtime']['aqi'])
        #未来天气
        for row in data['future']:
            result.append(data['city'] + "@" + row['date'][5:] + ": " + row['temperature'] + "℃\n白天" + wids[row['wid']['day']] +
                      ", 晚上" + wids[row['wid']['night']] + "\n" + data['realtime']['direct'])
        return result
    except Exception as arg:
        return [arg]

def table_exists(conn:sqlite3.Connection, table_name:str):
    c = conn.execute("SELECT COUNT(*) FROM sqlite_master where type='table' and name=?", (table_name, ))
    if c.fetchone()[0] == 0:
        return False
    return True