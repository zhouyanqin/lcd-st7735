#!/usr/bin/env python
#encoding: utf-8
import os
import time
import datetime
from PIL import ImageFont

from luma.core.interface.serial import spi
from luma.core.render import canvas
from luma.lcd.device import st7735

import host_status as host

LINEHIGHT = 12
def main():
    #serial = spi(port=0, device=0)
    serial = spi(port=1, device=2, gpio_DC=6, gpio_RST=26)
    device = st7735(serial, width=128, height=128, rotate=0, h_offset=1, v_offset=2, bgr=True, gpio_LIGHT=5, active_low=False)
    font = ImageFont.truetype(os.path.abspath(os.path.join(os.path.dirname(__file__), 'msyh.ttf')), size=10)

    #with canvas(device) as draw:
    #    draw.text((10, 40), 'start testing...')
    #device.backlight(True)
    #time.sleep(5)
    #device.backlight(False)
    #time.sleep(5)
    #device.backlight(True)
    # line = 0
    count = 0
    public_ip = '--'
    eth0 = '--'
    wlan0 = '--'
    weather = '--'
    weather_count = 0
    while True:
        with canvas(device) as draw:
            if weather_count == 0 or weather == 'Err':
                weather = host.get_weather()
            if count == 0:
                public_ip = host.get_public_ip()
                eth0 = host.get_ip_address("eth0")
                wlan0 = host.get_ip_address("wlan0")
            draw.font = font
            # 第一行显示时间
            draw.text((0, 0), text=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()), fill="green")

            # 第二行显示外网IP
            draw.text((0, LINEHIGHT), text="外网IP: " + public_ip, fill="red")

            # 第三行显示网卡ip
            draw.text((0, LINEHIGHT * 2), text="网卡IP: " + eth0)
            # 第四行显示网卡ip
            draw.text((0, LINEHIGHT * 3), text="无线IP: " + wlan0)

            # 第五行显示CPU占有率和温度
            #draw.text((0, LINEHIGHT * 4), text="CPU温度:" + host.get_cpu_temperture() + " CPU: " + host.get_cpu_percent(percpu= False))
            draw.text((0, LINEHIGHT * 4),
                      text="CPU温度:" + host.get_cpu_temperture())
            # 第六行显示CPU占有率
            #draw.text((0, LINEHIGHT * 5), text="CPU:" + host.get_cpu_percent(percpu=True))
            draw.text((0, LINEHIGHT * 5), text="CPU占有率:" + host.get_cpu_percent(percpu=False))

            # 第七行显示天气
            weather_line_count = len(weather)
            weather_line = weather[(weather_count // 5) % weather_line_count]
            w = [i.strip() for i in weather_line.split("\n", 2)]
            draw.text((0, LINEHIGHT * 6), text="".join(w[0:1]))
            draw.text((10, LINEHIGHT * 7), text="".join(w[1:2]))
            draw.text((10, LINEHIGHT * 8), text="".join(w[2:3]))
                # w = [i.strip() for i in weather.split(',', 2)]
                # draw.text((0, LINEHIGHT * 6), text="".join(w[0:1]))
                # draw.text((10, LINEHIGHT * 7), text="".join(w[1:2]))
                # draw.text((10, LINEHIGHT * 8), text="".join(w[2:3]))

            # 第十行显示运行时间
            draw.text((0, LINEHIGHT * 9), text="运行时间: " + host.get_up_time())
            
            # i = 0
            # while i <= line:
            #     draw.text((0, i * LINEHIGHT), text = '第' + str(i + 1) + '行')
            #     i += 1
            # line += 1
            # if line > 20:
            #     line = 0
            # draw.text((0, 0), text='中文!', fill='red')
            # draw.text((0, LINEHIGHT), text="第二行", font=font, fill="green")
            # draw.text((0, LINEHIGHT * 2), text="line3 with 中英文", fill=(255, 0, 0))
            count += 1
            if count > 20:
                #shutdown 
                count = 0
            weather_count += 1
            if weather_count > 1800:
                weather_count = 0
            time.sleep(1)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
